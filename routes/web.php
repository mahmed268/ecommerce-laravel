<?php

use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\ProductController;
use App\Http\Controllers\Frontend\CartController;
use App\Http\Controllers\Frontend\CheckoutController;
use App\Http\Controllers\Frontend\FrontendController;
use App\Http\Controllers\LoginController;
use Illuminate\Support\Facades\Route;


require __DIR__ . '/auth.php';

Route::middleware(['auth', 'isAdmin'])->group(function () {
    Route::get('/dashboard', function () {
        return view('backend.admin');
    })->name('dashboard');
    Route::get('/authenticated', [LoginController::class, 'authenticated']);

    // CategoryController
    Route::resource('categories', CategoryController::class);
    Route::get('/index', [CategoryController::class, 'index'])->name('categories.index');
    Route::get('/trashed-categories', [CategoryController::class, 'trash'])->name('categories.trash');
    Route::get('/trashed-categories/{category}/restore', [CategoryController::class, 'restore'])->name('categories.restore');
    Route::delete('/trashed-categories/{category}/delete', [CategoryController::class, 'delete'])->name('categories.delete');

    // ProductController
    Route::resource('products', ProductController::class);
    Route::get('/trashed-products', [ProductController::class, 'trash'])->name('products.trash');
    Route::get('/trashed-products/{product}/restore', [ProductController::class, 'restore'])->name('products.restore');
    Route::delete('/trashed-products/{product}/delete', [ProductController::class, 'delete'])->name('products.delete');
});

// FrontendController
Route::get('/', [FrontendController::class, 'index'])->name('home.index');
Route::get('/single-product', [FrontendController::class, 'singleProduct'])->name('home.singleProduct');
Route::get('/categories', [FrontendController::class, 'category'])->name('home.category');
Route::get('/categories-view/{slug}', [FrontendController::class, 'viewCategories'])->name('categories.view');
Route::get('/category/{cate_slug}/{prod_slug}', [FrontendController::class, 'singleProduct']);

// CartController
Route::POST('/add-to-cart', [CartController::class, 'addProduct']);
Route::POST('/delete-cart-item', [CartController::class, 'deleteProduct']);
Route::POST('/update-cart', [CartController::class, 'updateCart']);
Route::middleware(['auth'])->group(function () {
    Route::get('/cart', [CartController::class, 'viewCart'])->name('home.viewCart');

    // CheckoutController
    Route::get('/checkout', [CheckoutController::class, 'index'])->name('home.checkout');
});
