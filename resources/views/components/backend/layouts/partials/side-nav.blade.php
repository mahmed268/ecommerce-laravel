<div id="layoutSidenav_nav">
    <nav class="sb-sidenav accordion sb-sidenav-light shadow-lg border-0 rounded-lg" id="sidenavAccordion">
        <div class="sb-sidenav-menu">
            <div class="nav">
                <div class="sb-sidenav-menu-heading">Core</div>
                <a class="nav-link {{ Request::is('dashboard') ? 'active' : '' }}" href="index.html">
                    <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                    Dashboard
                </a>
                <a class="nav-link {{ Request::is('categories', 'categories/create') ? 'active' : '' }}"
                    href="{{ route('categories.index') }}">
                    <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                    Categories
                </a>
                <a class="nav-link {{ Request::is('products', 'products/create') ? 'active' : '' }}"
                    href="{{ route('products.index') }}">
                    <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                    Products
                </a>
            </div>
        </div>
        <div class="sb-sidenav-footer">
            <div class="small">Logged in as:</div>
            @if (auth()->user()->role_id == 0)
                Admin
            @endif
        </div>
    </nav>
</div>
