<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title')</title>
    <!-- Favicon-->
    <link rel="icon" type="image/x-icon" href="{{ asset('') }}ui/frontend/assets/favicon.ico" />
    <!-- Bootstrap icons-->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css" rel="stylesheet" />
    <!-- Core theme CSS (includes Bootstrap)-->
    <link href="{{ asset('') }}ui/frontend/css/styles.css" rel="stylesheet" />
    <link href="{{ asset('') }}frontend/css/custom.css" rel="stylesheet" />
    <!-- Owl Carosel-->
    <link href="{{ asset('') }}frontend/css/owl.carousel.css" rel="stylesheet" />
    <link href="{{ asset('') }}frontend/css/owl.theme.green.css" rel="stylesheet" />
    <!-- Google Font-->
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Libre+Baskerville&display=swap" rel="stylesheet">

</head>

<body>

    <!-- Navigation-->
    <x-frontend.layouts.partials.navbar />
    <!-- Section-->
    {{ $slot }}
    <!-- Bootstrap core JS-->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
    <!-- Core theme JS-->
    <script src="{{ asset('') }}frontend/js/jquery.min.js"></script>
    <script src="{{ asset('') }}frontend/js/owl.carousel.min.js"></script>
    <script src="{{ asset('') }}ui/frontend/js/scripts.js"></script>
    <script src="{{ asset('') }}frontend/js/custom.js"></script>

    @yield('carosel')
</body>


</html>
