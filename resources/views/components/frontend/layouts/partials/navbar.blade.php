<nav class="navbar navbar-expand-lg navbar-light" style="background-color: #86bfd1;">
    <div class="container px-4 px-lg-5">
        <a class="navbar-brand" href="{{ route('home.index') }}">M Ecom</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span
                class="navbar-toggler-icon"></span></button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0 ms-lg-4">
                <li class="nav-item"><a class="nav-link" href="{{ route('home.category') }}">Category</a>
                </li>
                <li class="nav-item"><a class="nav-link" href="{{ route('home.viewCart') }}">Cart</a>
                </li>
            </ul>
            @guest
                <ul class="navbar-nav me-auto mb-2 mb-lg-0 ms-lg-4">
                    @if (Route::has('login'))
                        <li class="nav-item">
                            <a class="nav-link" target="_blank" href="{{ route('login') }}">{{ __('Login') }}</a>
                        </li>
                    @endif

                    @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                        </li>
                    @endif
                </ul>
            @else
                <ul class="navbar-nav me-auto mb-2 mb-lg-0 ms-lg-4">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" id="navbarDropdown" href="#" role="button"
                            data-bs-toggle="dropdown" aria-expanded="false"><i class="fas fa-user fa-fw"></i>
                            {{ Auth::user()->name }}</a>
                        <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                            <form method="POST" action="{{ route('logout') }}">
                                @csrf
                                <li><a class="dropdown-item" href="route('logout')"
                                        onclick="event.preventDefault();
                                                                                                                                                    this.closest('form').submit();">Logout</a>
                                </li>
                            </form>
                        </ul>
                    </li>
                </ul>
            @endguest
        </div>
    </div>
</nav>
