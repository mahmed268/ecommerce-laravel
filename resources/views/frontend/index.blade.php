<x-frontend.layouts.master>
    @section('title')
        Home Page
    @endsection
    <x-frontend.layouts.partials.slider />
    <section class="py-5 ">
        <div class="container px-4 px-lg-5 mt-5">
            <h2 class="mb-3">Featured Products</h2>
            <div class="row gx-4 gx-lg-5 row-cols-2 row-cols-md-3 row-cols-xl-4 justify-content-center">
                <div class="owl-carousel owl-theme">
                    @foreach ($featured_products as $product)
                        <div class="col mb-5">
                            <div class="card h-100">
                                <!-- Product image-->
                                <img class="card-img-top owl-img"
                                    src="{{ asset('storage/images/product/' . $product->image) }}" alt="..." />
                                <!-- Product details-->
                                <div class="card-body p-4">
                                    <div class="text-center">
                                        <!-- Product name-->
                                        <h5 class="fw-bolder">{{ $product->name }}</h5>
                                        <!-- Product price-->
                                        <span
                                            class="float-start text-muted text-decoration-line-through">TK.{{ $product->original_price }}</span>
                                        <span class="float-end">TK.{{ $product->selling_price }}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="container px-4 px-lg-5 mt-5">
            <h2 class="mb-3">Trending Categories</h2>
            <div class="row gx-4 gx-lg-5 row-cols-2 row-cols-md-3 row-cols-xl-4 justify-content-center">
                <div class="owl-carousel owl-theme">
                    @foreach ($trending_categories as $category)
                        <div class="col mb-5">
                            <a href="{{ url('categories-view/' . $category->slug) }}">
                                <div class="card h-100">
                                    <!-- Category image-->
                                    <img class="card-img-top owl-img"
                                        src="{{ asset('storage/images/category/' . $category->image) }}" alt="..." />
                                    <!-- Category details-->
                                    <div class="card-body p-4 font-color">
                                        <div class="text-center">
                                            <!-- Category name-->
                                            <h5 class="fw-bolder">{{ $category->name }}</h5>
                                            <!-- Category price-->
                                            <span>
                                                <p>{{ $category->description }}</p>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
    @section('carosel')
        <script>
            $(document).ready(function() {
                $('.owl-carousel').owlCarousel({
                    loop: true,
                    margin: 10,
                    nav: true,
                    responsive: {
                        0: {
                            items: 1
                        },
                        600: {
                            items: 3
                        },
                        1000: {
                            items: 5
                        }
                    }
                })
            })
        </script>
    @endsection
</x-frontend.layouts.master>
