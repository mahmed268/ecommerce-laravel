<x-frontend.layouts.master>
    @section('title')
        {{ $product->name }}
    @endsection
    <section class="py-2">
        <div class="container px-4 px-lg-5 my-2 shadow p-3 mb-5 bg-white rounded">
            <div class="py-3 mb-4 shadow-sm bg-warning border-top">
                <div class="container">
                    <h5 class="mb-0">
                        <a class="font-color" href="{{ url('categories') }}"> Collections</a> /
                        <a class="font-color" href="{{ url('categories-view/' . $product->category->slug) }}">
                            {{ $product->category->name }} </a> /
                        {{ $product->name }}
                    </h5>
                </div>
            </div>
            <div class="row gx-4 gx-lg-5 align-items-center product_data">
                <div class="col-md-4"><img class="w-75 card-img-top mb-5 mb-md-0"
                        src="{{ asset('storage/images/product/' . $product->image) }}" alt="..." /></div>
                <div class="col-md-8">
                    <h1 class="display-5 fw-bolder">{{ $product->name }}
                        <label style="font-size: 16px;"
                            class="float-end badge bg-danger">{{ $product->trending == '1' ? 'Trending' : '' }}</label>
                    </h1>
                    <hr>
                    <div class="fs-5 mb-5">
                        <span class="me-3">Original Price:
                            <s>TK. {{ $product->original_price }}</s></span> <br>
                        <span class="fw-bold">Selling Price: TK. {{ $product->selling_price }}</span>
                    </div>
                    <p class="lead">{!! $product->small_description !!}</p>
                    <hr>
                    @if ($product->quantity > 0)
                        <label class="badge bg-success">In Stock</label>
                    @else
                        <label class="badge bg-danger">Out of Stock</label>
                    @endif
                    <div class="row mt-2">
                        <div class="col-md-2">
                            <input type="hidden" value="{{ $product->id }}" class="prod_id">
                            <label for="Quantity">Quantity</label>
                            <div class="input-group text-center mb-3">
                                <button class="input-group-text decrement-btn">-</button>
                                <input type="text" name="quantity" value="1" class="form-control qty-input" />
                                <button class="input-group-text increment-btn">+</button>
                            </div>
                        </div>
                        <div class="col-md-10">
                            <br>
                            @if ($product->quantity > 0)
                                <button class="btn btn-primary addToCartBtn me-3 float-start">Add to Cart <i
                                        class="bi bi-cart-fill"></i></button>
                            @endif
                            <button class="btn btn-success me-3 float-start">Add to Wishlist <i
                                    class="bi bi-heart-fill"></i>
                            </button>

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <hr>
                <h3>Description</h3>
                <p class="mt-3">
                    {!! $product->description !!}
                </p>
            </div>
        </div>
    </section>

</x-frontend.layouts.master>
