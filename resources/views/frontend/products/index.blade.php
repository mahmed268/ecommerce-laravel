<x-frontend.layouts.master>
    @section('title')
        {{ $category->name }}
    @endsection
    <section class="py-3 ">
        <div class="container px-4 px-lg-5 mt-2 shadow p-3 mb-5 bg-white rounded">
            <div class="py-3 mb-4 shadow-sm bg-warning border-top">
                <div class="container">
                    <h5 class="mb-0">
                        <a class="font-color" href="{{ url('categories') }}"> Collections</a>
                        / {{ $category->name }}
                    </h5>
                </div>
            </div>
            <h2 class="mb-3">{{ $category->name }}</h2>
            <div class="row gx-4 gx-lg-5 row-cols-2 row-cols-md-3 row-cols-xl-4 justify-content-center">
                @foreach ($products as $product)
                    <div class="col mb-5">
                        <div class="card h-100">
                            <a href="{{ url('category/' . $category->slug . '/' . $product->slug) }}">
                                <!-- Product image-->
                                <img class="card-img-top owl-img"
                                    src="{{ asset('storage/images/product/' . $product->image) }}" alt="..." />
                                <!-- Product details-->
                                <div class="card-body p-4 font-color">
                                    <div class="text-center">
                                        <!-- Product name-->
                                        <h5 class="fw-bolder">{{ $product->name }}</h5>
                                        <!-- Product price-->
                                        <span
                                            class="float-start text-muted text-decoration-line-through">TK.{{ $product->original_price }}</span>
                                        <span class="float-end">TK.{{ $product->selling_price }}</span>

                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                @endforeach
            </div>
            {{ $products->links() }}
        </div>
    </section>
</x-frontend.layouts.master>
