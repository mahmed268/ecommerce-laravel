<x-frontend.layouts.master>
    @section('title')
        My Cart
    @endsection
    <section class="py-2">
        <div class="container px-4 px-lg-5 my-2 shadow p-3 mb-5 bg-white rounded card">
            <div class="py-3 mb-4 shadow-sm bg-warning border-top">
                <div class="container">
                    <h5 class="mb-0">
                        <a class="font-color" href="{{ url('/') }}"> Home</a> /
                        <a class="font-color" href="{{ url('cart') }}">
                            Cart </a>
                    </h5>
                </div>
            </div>
            @php
                $total = 0;
            @endphp
            @foreach ($cartItems as $item)
                <div class="row gx-4 gx-lg-5 align-items-center product_data cart-body">
                    <div class="col-md-2 my-auto">
                        <img src="{{ asset('storage/images/product/' . $item->products->image) }}" height="70px"
                            alt="">
                    </div>
                    <div class="col-md-3 my-auto">
                        <h5>{{ $item->products->name }}</h5>
                    </div>
                    <div class="col-md-2 my-auto">
                        <h5>Tk. {{ $item->products->selling_price }}</h5>
                    </div>
                    <div class="col-md-3 my-auto">
                        <input type="hidden" class="prod_id" value="{{ $item->prod_id }}">
                        @if ($item->products->quantity > $item->prod_qty)
                            <label for="Quantity">Quantity</label>
                            <div class="input-group text-center mb-3" style="width: 130px">
                                <button class="input-group-text change-quantity decrement-btn">-</button>
                                <input type="text" name="quantity" value="{{ $item->prod_qty }}"
                                    class="form-control qty-input" />
                                <button class="input-group-text change-quantity increment-btn">+</button>
                            </div>
                            @php
                                $total += $item->products->selling_price * $item->prod_qty;
                            @endphp
                        @else
                            <h6 style="color: red">Out of Stock</h6>
                        @endif
                    </div>
                    <div class="col-md-2 my-auto">
                        <button class="btn btn-danger delete-cart-item"> <i class="bi bi-trash"></i> Remove</button>
                    </div>
                </div>
            @endforeach
            <hr>
            <div class="cart-footer">
                <h6>Total Price: Tk. {{ $total }}
                    <a href="{{ url('checkout') }}" class="btn btn-outline-success float-end">Proceed to Checkout</a>
                </h6>
            </div>
        </div>
    </section>
</x-frontend.layouts.master>
