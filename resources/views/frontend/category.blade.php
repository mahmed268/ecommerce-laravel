<x-frontend.layouts.master>
    @section('title')
        Category Page
    @endsection
    <section class="py-5 ">
        <div class="container px-4 px-lg-5 mt-5">
            <h2 class="mb-3">All Categories</h2>
            <div class="row gx-4 gx-lg-5 row-cols-2 row-cols-md-3 row-cols-xl-4 justify-content-center">
                @foreach ($categories as $category)
                    <div class="col mb-5">
                        <a href="{{ url('categories-view/' . $category->slug) }}">
                            <div class="card h-100">
                                <!-- Category image-->
                                <img class="card-img-top owl-img"
                                    src="{{ asset('storage/images/category/' . $category->image) }}" alt="..." />
                                <!-- Category details-->
                                <div class="card-body p-4">
                                    <div class="text-center">
                                        <!-- Category name-->
                                        <h5 class="fw-bolder">{{ $category->name }}</h5>
                                        <!-- Category price-->
                                        <span>
                                            <p>{{ $category->description }}</p>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                @endforeach
            </div>
            {{ $categories->links() }}
        </div>
    </section>
</x-frontend.layouts.master>
