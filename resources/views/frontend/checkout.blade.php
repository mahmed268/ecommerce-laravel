<x-frontend.layouts.master>
    @section('title')
        Checkout
    @endsection
    <section class="py-2">
        <div class="container px-4 px-lg-5 my-2 shadow p-3 mb-5 bg-white rounded card">
            <div class="py-3 mb-4 shadow-sm bg-warning border-top">
                <div class="container">
                    <h5 class="mb-0">
                        <a class="font-color" href="{{ url('/') }}"> Home</a> /
                        <a class="font-color" href="{{ url('checkout') }}">
                            Checkout </a>
                    </h5>
                </div>
            </div>
            <div class="row gx-4 gx-lg-5 align-items-center product_data cart-body">
                <div class="col-md-7">
                    <div class="card">
                        <div class="card-body">
                            <h6>Basic Details</h6>
                            <hr>
                            <div class="row checkout-form">
                                <div class="col-md-12">
                                    <label for="firstName">Full Name</label>
                                    <input type="text" class="form-control" placeholder="Enter First Name">
                                </div>
                                <div class="col-md-6 mt-3">
                                    <label for="email">Email Address</label>
                                    <input type="email" class="form-control" placeholder="Enter Email Address">
                                </div>
                                <div class="col-md-6 mt-3">
                                    <label for="number">Phone Number</label>
                                    <input type="text" class="form-control" placeholder="Enter Phone Number">
                                </div>
                                <div class="col-md-12 mt-3">
                                    <label for="text">Address</label>
                                    <input type="text" class="form-control" placeholder="Enter Address 1">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="card">
                        <div class="card-body">
                            <h6>Order Details</h6>
                            <hr>
                            <table class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <td>Product Name</td>
                                        <td>Quantity</td>
                                        <td>Price</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($cartItems as $item)
                                        <tr>
                                            <td>{{ $item->products->name }}</td>
                                            <td>{{ $item->prod_qty }}</td>
                                            <td>{{ $item->products->selling_price }}</td>

                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <hr>
                            <button class="btn btn-primary float-end"> Place Order </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</x-frontend.layouts.master>
