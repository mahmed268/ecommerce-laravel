<x-backend.layouts.master>
    <main>
        <div class="row justify-content-center">
            <div class="col-lg-10">
                @if ($errors->any('message'))
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="card shadow-lg border-0 rounded-lg mt-5">
                    <div class="card-header">
                        <h3 class="text-center font-weight-light my-4">Edit Category</h3>
                    </div>
                    <div class="card-body">
                        <form action="{{ route('categories.update', ['category' => $category->id]) }}" method="POST"
                            enctype="multipart/form-data">
                            @method('patch')
                            @csrf
                            <div class="row mb-3">
                                <div class="col-md-6">
                                    <div class="form-floating mb-3 mb-md-0">
                                        <input class="form-control" id="" type="text" name="name"
                                            value="{{ $category->name }}" placeholder="Enter your Category name" />
                                        <label for="">Category name</label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-floating mb-3 mb-md-0">
                                        <input class="form-control" id="" type="text" name="slug"
                                            value="{{ $category->slug }}" placeholder="Enter your Slug" />
                                        <label for="">Slug</label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-floating mb-3">
                                <textarea class="form-control" name="description" id="" rows="5"
                                    placeholder="Category Description">{{ $category->description }}</textarea>
                                <label for="">Category Description</label>
                            </div>
                            <div class="row mb-3">
                                <div class="col-md-6">
                                    <div class="mb-3 mb-md-0">
                                        <input id="" name="status" type="checkbox"
                                            {{ $category->status == '1' ? 'checked' : '' }} />
                                        <label for="">Status</label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="mb-3 mb-md-0">
                                        <input id="" name="popular" type="checkbox"
                                            {{ $category->popular == '1' ? 'checked' : '' }} />
                                        <label for="">Popular</label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-floating mb-3">
                                <input class="form-control" id="" name="meta_title" type="text"
                                    placeholder="Meta Title" value="{{ $category->meta_title }}" />
                                <label for="">Meta Title</label>
                            </div>
                            <div class="form-floating mb-3">
                                <textarea class="form-control" name="meta_keywords" id="" rows="3"
                                    placeholder="Meta Keywords">{{ $category->meta_keywords }}</textarea>
                                <label for="">Meta Keywords</label>
                            </div>
                            <div class="form-floating mb-3">
                                <textarea class="form-control" name="meta_description" id="" rows="3"
                                    placeholder="Meta Description">{{ $category->meta_description }}</textarea>
                                <label for="">Meta Description</label>
                            </div>
                            <div class="form-floating mb-3 mb-md-0">
                                @if ($category->image)
                                    <img class="edit-img"
                                        src="{{ asset('storage/images/category/' . $category->image) }}" />
                                @endif
                            </div>
                            <div class="form-floating mb-3 mb-md-0">
                                <input id="" type="file" name="image" />
                            </div>

                            <div class="mt-4 mb-0">
                                <div class="float-end">
                                    <button class="btn btn-primary btn-block" type="submit">Update</button>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </main>
</x-backend.layouts.master>
