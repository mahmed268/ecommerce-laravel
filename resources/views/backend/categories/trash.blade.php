<x-backend.layouts.master>
    <main>
        <div class="container-fluid px-4">
            <h2 class="mt-4">Category Page</h2>
            @if ($errors->any('message'))
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="card mb-4">
                <div class="card-header">
                    <i class="fas fa-table me-1"></i>
                    Trash Category Table
                    <a class="btn btn-primary btn-sm" href="{{ route('categories.index') }}">Category List Page</a>
                    <a class="btn btn-info btn-sm" href="{{ route('categories.create') }}">Add Category</a>
                </div>
                <div class="card-body">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Category Name</th>
                                <th scope="col">Description</th>
                                <th scope="col">Image</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php $sl=0 @endphp
                            @foreach ($categories as $category)
                                <tr>
                                    <th scope="row">{{ ++$sl }}</th>
                                    <td>{{ $category->name }}</td>
                                    <td>{{ $category->description }}</td>
                                    <td><img class="w-25"
                                            src="{{ asset('storage/images/category/' . $category->image) }}" /></td>
                                    <td>
                                        <a class="btn btn-info btn-sm"
                                            href="{{ route('categories.restore', ['category' => $category->id]) }}">Restore</a>
                                        <form style="display:inline"
                                            action="{{ route('categories.delete', ['category' => $category->id]) }}"
                                            method="post">
                                            @csrf
                                            @method('delete')
                                            <button onclick="return confirm('Are you sure want to delete parmanently?')"
                                                class="btn btn-sm btn-danger" type="submit">Parmanent Delete</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{ $categories->links() }}
                </div>
            </div>
        </div>
    </main>
</x-backend.layouts.master>
