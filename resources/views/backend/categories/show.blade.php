<x-backend.layouts.master>
    <main>
        <div class="container-fluid px-4">
            <h2 class="mt-4">Category Details Page</h2>
            <div class="card-body">
                <h2>Title: {{ $category->name }}</h2>
                <p>Description: {{ $category->description }}</p>
                <p>Status:
                    @if ($category->status == 1)
                        {{ 'Active' }}
                    @else
                        {{ 'Deactive' }}
                    @endif
                </p>
                <img class="show-img" src="{{ asset('storage/images/category/' . $category->image) }}" />
            </div>
        </div>
    </main>
</x-backend.layouts.master>
