<x-backend.layouts.master>
    <main>
        <div class="container-fluid px-4">
            <h2 class="mt-4">Category Page</h2>
            <div class="card mb-4">
                <div class="card-header">
                    <i class="fas fa-table me-1"></i>
                    Category Table
                    <a class="btn btn-primary btn-sm" href="{{ route('categories.create') }}">Add Category</a>
                    <a class="btn btn-warning btn-sm" href="{{ route('categories.trash') }}">Trash Page</a>
                </div>
                @if ($errors->any('message'))
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="card-body">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Category Name</th>
                                <th scope="col">Description</th>
                                <th scope="col">Image</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php $sl=0 @endphp
                            @foreach ($categories as $category)
                                <tr>
                                    <th scope="row">{{ ++$sl }}</th>
                                    <td>{{ $category->name }}</td>
                                    <td>{{ $category->description }}</td>
                                    <td><img class="index-img"
                                            src="{{ asset('storage/images/category/' . $category->image) }}" /></td>
                                    <td>
                                        <a class="btn btn-info btn-sm"
                                            href="{{ route('categories.show', ['category' => $category->id]) }}">Show</a>
                                        <a class="btn btn-warning btn-sm"
                                            href="{{ route('categories.edit', ['category' => $category->id]) }}">Edit</a>
                                        <form style="display:inline"
                                            action="{{ route('categories.destroy', ['category' => $category->id]) }}"
                                            method="post">
                                            @csrf
                                            @method('delete')
                                            <button onclick="return confirm('Are you sure want to delete ?')"
                                                class="btn btn-sm btn-danger" type="submit">Delete</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{ $categories->links() }}
                </div>
            </div>
        </div>
    </main>
</x-backend.layouts.master>
