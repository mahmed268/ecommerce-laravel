<x-backend.layouts.master>
    <main>
        <div class="container-fluid px-4">
            <h2 class="mt-4">Product Page</h2>
            <div class="card mb-4">
                <div class="card-header">
                    <i class="fas fa-table me-1"></i>
                    Product Table
                    <a class="btn btn-primary btn-sm" href="{{ route('products.create') }}">Add Product</a>
                    <a class="btn btn-warning btn-sm" href="{{ route('products.trash') }}">Trash Page</a>
                </div>

                <div class="card-body">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Category Name</th>
                                <th scope="col">Product Name</th>
                                <th scope="col">Description</th>
                                <th scope="col">Image</th>
                                <th scope="col">Selling Price</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php $sl=0 @endphp
                            @foreach ($products as $product)
                                <tr>
                                    <th scope="row">{{ ++$sl }}</th>
                                    <td>{{ $product->category->name }}</td>
                                    <td>{{ $product->name }}</td>
                                    <td>{{ $product->description }}</td>
                                    <td><img class="index-img"
                                            src="{{ asset('storage/images/product/' . $product->image) }}" />
                                    </td>
                                    {{-- <td>{{ $product->original_price - $product->discount }}</td> --}}
                                    <td>{{ $product->selling_price }}</td>
                                    <td>
                                        <a class="btn btn-info btn-sm"
                                            href="{{ route('products.show', ['product' => $product->id]) }}">Show</a>
                                        <a class="btn btn-warning btn-sm"
                                            href="{{ route('products.edit', ['product' => $product->id]) }}">Edit</a>
                                        <form style="display:inline"
                                            action="{{ route('products.destroy', ['product' => $product->id]) }}"
                                            method="post">
                                            @csrf
                                            @method('delete')
                                            <button onclick="return confirm('Are you sure want to delete ?')"
                                                class="btn btn-sm btn-danger" type="submit">Delete</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{ $products->links() }}
                </div>
            </div>
        </div>
    </main>
</x-backend.layouts.master>
