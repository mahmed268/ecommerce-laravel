<x-backend.layouts.master>
    <main>
        <div class="container-fluid px-4">
            <h2 class="mt-4">Product Page</h2>
            @if ($errors->any('message'))
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="card mb-4">
                <div class="card-header">
                    <i class="fas fa-table me-1"></i>
                    Trash Product Table
                    <a class="btn btn-primary btn-sm" href="{{ route('products.index') }}">Product List Page</a>
                    <a class="btn btn-info btn-sm" href="{{ route('products.create') }}">Add Product</a>
                </div>
                <div class="card-body">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Product Name</th>
                                <th scope="col">Description</th>
                                <th scope="col">Image</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php $sl=0 @endphp
                            @foreach ($products as $product)
                                <tr>
                                    <th scope="row">{{ ++$sl }}</th>
                                    <td>{{ $product->name }}</td>
                                    <td>{{ $product->description }}</td>
                                    <td><img class="w-25"
                                            src="{{ asset('storage/images/product/' . $product->image) }}" /></td>
                                    <td>
                                        <a class="btn btn-info btn-sm"
                                            href="{{ route('products.restore', ['product' => $product->id]) }}">Restore</a>
                                        <form style="display:inline"
                                            action="{{ route('products.delete', ['product' => $product->id]) }}"
                                            method="post">
                                            @csrf
                                            @method('delete')
                                            <button onclick="return confirm('Are you sure want to delete parmanently?')"
                                                class="btn btn-sm btn-danger" type="submit">Parmanent Delete</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{ $products->links() }}
                </div>
            </div>
        </div>
    </main>
</x-backend.layouts.master>
