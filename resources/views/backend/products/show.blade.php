<x-backend.layouts.master>
    <main>
        <div class="container-fluid px-4">
            <h2 class="mt-4">Products Details Page</h2>
            <div class="card-body">
                <h2>Category: {{ $product->category->name }}</h2>
                <h3>Title: {{ $product->name }}</h3>
                <p>Description: {{ $product->description }}</p>
                <p>Description: {{ $product->selling_price }}</p>
                <p>Status:
                    @if ($product->status == 1)
                        {{ 'Active' }}
                    @else
                        {{ 'Deactive' }}
                    @endif
                </p>
                <img class="show-img" src="{{ asset('storage/images/product/' . $product->image) }}" />
            </div>
        </div>
    </main>
</x-backend.layouts.master>
