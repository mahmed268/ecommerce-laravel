<x-backend.layouts.master>
    <main>
        <div class="row justify-content-center">
            <div class="col-lg-10">
                @if ($errors->any('message'))
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="card shadow-lg border-0 rounded-lg mt-5">
                    <div class="card-header">
                        <h3 class="text-center font-weight-light my-4">Edit Category</h3>
                    </div>
                    <div class="card-body">
                        <form action="{{ route('products.update', ['product' => $product->id]) }}" method="POST"
                            enctype="multipart/form-data">
                            @method('patch')
                            @csrf
                            <div class="row mb-3">
                                <div class="col-md-6">
                                    <div class="form-floating mb-3 mb-md-0">
                                        <select name="cate_id" id="" class="form-select">
                                            <option disabled>Select Category</option>
                                            <option value="">{{ $product->category->name }}
                                            </option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-floating mb-3">
                                <input class="form-control" id="" name="name" type="text"
                                    value="{{ $product->name }}" placeholder="Product Name" />
                                <label for="">Product Name</label>
                            </div>
                            <div class="form-floating mb-3">
                                <input class="form-control" id="" name="slug" type="text"
                                    value="{{ $product->slug }}" placeholder="Product Slug" />
                                <label for="">Product Slug</label>
                            </div>
                            <div class="form-floating mb-3">
                                <textarea class="form-control" name="small_description" id="" rows="3"
                                    placeholder="Product Small Description">{{ $product->small_description }}</textarea>
                                <label for="">Product Small Description</label>
                            </div>
                            <div class="form-floating mb-3">
                                <textarea class="form-control" name="description" id="" rows="5"
                                    placeholder="Product Description">{{ $product->description }}</textarea>
                                <label for="">Product Description</label>
                            </div>
                            <div class="row mb-3">
                                <div class="col-md-6">
                                    <div class="form-floating mb-3">
                                        <input class="form-control" id="" name="original_price" type="text"
                                            onkeypress="return onlyNumberKey(event)"
                                            value="{{ $product->original_price }}" placeholder="Original Price" />
                                        <label for="">Original Price</label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-floating mb-3">
                                        <input class="form-control" id="" name="selling_price" type="text"
                                            onkeypress="return onlyNumberKey(event)"
                                            value="{{ $product->selling_price }}" placeholder="Selling Price" />
                                        <label for="">Selling Price</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-md-6">
                                    <div class="form-floating mb-3">
                                        <input class="form-control" id="" name="tax" type="text"
                                            onkeypress="return onlyNumberKey(event)" value="{{ $product->tax }}"
                                            placeholder="Tax" />
                                        <label for="">Tax</label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-floating mb-3">
                                        <input class="form-control" id="" name="quantity" type="text"
                                            onkeypress="return onlyNumberKey(event)" value="{{ $product->quantity }}"
                                            placeholder="Quantity" />
                                        <label for="">Quantity</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-md-6">
                                    <div class="mb-3 mb-md-0">
                                        <input id="" name="status" type="checkbox"
                                            {{ $product->status == '1' ? 'checked' : '' }} />
                                        <label for="">Status</label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="mb-3 mb-md-0">
                                        <input id="" name="trending" type="checkbox"
                                            {{ $product->trending == '1' ? 'checked' : '' }} />
                                        <label for="">Trending</label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-floating mb-3">
                                <input class="form-control" id="" name="meta_title" type="text"
                                    placeholder="Meta Title" value="{{ $product->meta_title }}" />
                                <label for="">Meta Title</label>
                            </div>
                            <div class="form-floating mb-3">
                                <textarea class="form-control" name="meta_keywords" id="" rows="3"
                                    placeholder="Meta Keywords">{{ $product->meta_keywords }}</textarea>
                                <label for="">Meta Keywords</label>
                            </div>
                            <div class="form-floating mb-3">
                                <textarea class="form-control" name="meta_description" id="" rows="3"
                                    placeholder="Meta Description">{{ $product->meta_description }}</textarea>
                                <label for="">Meta Description</label>
                            </div>
                            <div class="form-floating mb-3 mb-md-0">
                                @if ($product->image)
                                    <img class="edit-img"
                                        src="{{ asset('storage/images/product/' . $product->image) }}" />
                                @endif
                            </div>
                            <div class="form-floating mb-3 mb-md-0">
                                <input id="" type="file" name="image" />
                            </div>

                            <div class="mt-4 mb-0">
                                <div class="float-end">
                                    <button class="btn btn-primary btn-block" type="submit">Update</button>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </main>
</x-backend.layouts.master>
