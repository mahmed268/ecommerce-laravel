<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Product;

class FrontendController extends Controller
{
    public function index()
    {
        $featured_products = Product::where('trending', '1')
            ->latest()
            ->take(15)
            ->get();
        $trending_categories = Category::where('popular', '1')
            ->take(15)
            ->get();
        return view('frontend.index', compact('featured_products', 'trending_categories'));
    }

    public function category()
    {
        $categories = Category::where('status', '0')
            ->latest()
            ->paginate(8);
        return view('frontend.category', compact('categories'));
    }

    public function viewCategories($slug)
    {
        if (Category::where('slug', $slug)->exists()) {
            $category = Category::where('slug', $slug)->first();
            $products = Product::where('cate_id', $category->id)->where('status', '0')
                ->latest()
                ->paginate(8);
            return view('frontend.products.index', compact('category', 'products'));
        } else {
            return redirect('/')->withMessage('Slug not exist');
        }
    }

    public function singleProduct($cate_slug, $prod_slug)
    {
        if (Category::where('slug', $cate_slug)->exists()) {
            if (Product::where('slug', $prod_slug)->exists()) {
                $product = Product::where('slug', $prod_slug)->first();
                return view('frontend.products.single-product', compact('product'));
            } else {
                return redirect('/')->withMessage('The link was broken');
            }
        } else {
            return redirect('/')->withMessage('Category not found');
        }
    }
}
