<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProductRequest;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\File;
use Image;


class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::latest()->paginate('10');
        return view('backend.products.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::orderBy('name', 'asc')
            ->pluck('name', 'id')
            ->toArray();
        return view('backend.products.create', compact('categories'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request)
    {
        try {
            Product::create([
                'cate_id' => $request->cate_id,
                'name' => $request->name,
                'slug' => $request->slug,
                'small_description' => $request->small_description,
                'description' => $request->description,
                'original_price' => $request->original_price,
                'selling_price' => $request->selling_price,
                'tax' => $request->tax,
                'quantity' => $request->quantity,
                'status' => $request->status == TRUE ? '1' : '0',
                'trending' => $request->trending == TRUE ? '1' : '0',
                'meta_title' => $request->meta_title,
                'meta_keywords' => $request->meta_keywords,
                'meta_description' => $request->meta_description,
                'image' => $this->uploadImage(request()->file('image'))
            ]);

            return redirect()->route('products.index')->withMessage('Successfully Created!');
        } catch (QueryException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        return view('backend.products.show', compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        return view('backend.products.edit', compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProductRequest $request, Product $product)
    {
        try {
            $requestData = [
                'name' => $request->name,
                'slug' => $request->slug,
                'small_description' => $request->small_description,
                'description' => $request->description,
                'original_price' => $request->original_price,
                'selling_price' => $request->selling_price,
                'tax' => $request->tax,
                'quantity' => $request->quantity,
                'status' => $request->status == TRUE ? '1' : '0',
                'trending' => $request->trending == TRUE ? '1' : '0',
                'meta_title' => $request->meta_title,
                'meta_keywords' => $request->meta_keywords,
                'meta_description' => $request->meta_description,
            ];
            if ($request->hasFile('image')) {
                $path = storage_path() . '/app/public/images/product/' . $product->image;
                if (File::exists($path)) {
                    File::delete($path);
                }
                $requestData['image'] = $this->uploadImage(request()->file('image'));
            }
            $product->update($requestData);

            return redirect()->route('products.index')->withMessage('Successfully Updated!');
        } catch (QueryException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        try {
            $product->delete();
            return redirect()->route('products.index')->withMessage('Successfully Deleted!');
        } catch (QueryException $e) {
            return redirect()->back()->withErrors($e->getMessage());
        }
    }

    public function uploadImage($file)
    {
        $fileName = time() . '.' . $file->getClientOriginalExtension();
        Image::make($file)
            // ->resize(200, 200)
            ->save(storage_path() . '/app/public/images/product/' . $fileName);
        return $fileName;
    }

    public function trash()
    {
        $productsCollection = Product::onlyTrashed()->latest();
        $products = $productsCollection->paginate('10');
        return view('backend.products.trash', compact('products'));
    }

    public function restore($id)
    {
        try {
            $product = Product::onlyTrashed()->findOrFail($id);
            $product->restore();
            return redirect()->route('products.trash')->withMessage('Successfully Restored!');
        } catch (QueryException $e) {
            return redirect()->back()->withErrors($e->getMessage());
        }
    }

    public function delete($id)
    {
        try {
            $product = Product::onlyTrashed()->findOrFail($id);
            if ($product->image) {
                $path = storage_path() . '/app/public/images/product/' . $product->image;
                if (File::exists($path)) {
                    File::delete($path);
                }
                $product->forceDelete();
            }
            return redirect()->route('products.trash')->withMessage('Successfully Permanently Deleted!');
        } catch (QueryException $e) {
            return redirect()->back()->withErrors($e->getMessage());
        }
    }
}
