<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $productId = $this->route('product')->id ?? '';
        if (request()->isMethod('post')) {
            $imageValidationRule = 'required|mimes:jpeg,png,jpg,gif|max:2048';
        } elseif (request()->isMethod('patch')) {
            $imageValidationRule = 'sometimes';
        }

        return [
            'name' => 'required|min:3|unique:categories,name,' . $productId,
            'slug' => 'required',
            'description' => 'required',
            'original_price' => 'required|numeric',
            'selling_price' => 'required|numeric',
            'tax' => 'numeric',
            'image' => $imageValidationRule,
            'meta_title' => 'required',
            'meta_description' => 'required',
            'meta_keywords' => 'required',
        ];
    }
}
