<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $categoryId = $this->route('category')->id ?? '';
        if (request()->isMethod('post')) {
            $imageValidationRule = 'required|mimes:jpeg,png,jpg,gif|max:2048';
        } elseif (request()->isMethod('patch')) {
            $imageValidationRule = 'sometimes';
        }

        return [
            'name' => 'required|min:3|unique:categories,name,' . $categoryId,
            'slug' => 'required',
            'description' => 'required',
            'image' => $imageValidationRule,
            'meta_title' => 'required',
            'meta_description' => 'required',
            'meta_keywords' => 'required',
        ];
    }
}
